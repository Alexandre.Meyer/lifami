---
title: "Emploi du temps et Examens"
description: "Emploi du temps et examens"
---



## Emploi du temps printemps 2025

![](../images/2024_2025_edt.png)

## Groupes

Les salles du vendredi.

* Groupe A : Quai 43 s.102 (1er étage) => Alexandre MEYER
* Groupe B : Quai 43 s.105 (1er étage) => Elodie DESSEREE
* Groupe C : Quai 43 s.106 (1er étage) => Marie LEFEVRE (+ Chloé CONRAD en TP de mini-projet)
* Groupe D : Quai 43 s.109 (1er étage) + Ariane 3 et 4 => Sébastien GADRAT (+ Gael CARNIEL en TP)
* Groupe E : Quai 43 s.111 (1er étage) + Ariane 5 et 6 => Quentin DESCHAMPS (uniquement Timon DESCHAMPS en TP)  
* Groupe F : Berthollet 203 (2e étage) => Eliane PERNA
* Groupe G : Berthollet 204 (2e étage) => Erwan GUILLOU + (Nicolas PRONOST en TP)

Pour les salles du lundi, voir dans TOMUSS.

## Examens

* Interro 1 : vendredi 07 février     (7% de la note finale)
* TP noté 1 : vendredi 28 février     (8% de la note finale)
* TP noté 2 : vendredi 04 avril       (20% de la note finale)
* Rendus du mini-projets OU TP Noté 3 (voir explication en CM)
  * Il faut avoir fait les 3 premiers évaluations et être présent en TD pour être noté au mini-projet, sinon vous aurez un TP noté à la place
  * vendredi 29 mars : nom du projet et thème validé par un enseignant
  * vendredi ? avril : intermédaire  (2% de la note finale)
  * vendredi ? avril : intermédaire (8% de la note finale)
  * vendredi ? avril : final        (15% de la note finale)
* Vendredi ?? ?? à 8h : rattrapage pour remplacer les absences
* Examen la semaine du 05 mai       (40% de la note finale)
