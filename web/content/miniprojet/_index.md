
---
title: "Mini-projet"
description: "Moni-projet"
---



## Sujet
Les séances de TP des N dernières semaines sont consacrés à la réalisation d'un mini-projet au choix. Chaque étudiant choisit un des thèmes vu plus ou moins durant les TD/TP parmi les idées suivantes.

#### Système de particules et maillage masses-ressorts
* Définition d'un maillage masse-ressort : ligne de ressorts, ressorts en forme de carré avec diagonale, cercle (une particule centrale+des particules autour), etc.
* L'utilisateur en cliquant bouge la particule la plus proche de la souris et influence la simulation.

#### Système de particule et mini-jeu de soucoupe volante incluant la gravitation universelle
* Une idée de sujet est décrite dans le poly de TD.

#### Simulation : proies / prédateurs au complet
* Simulation des loups, lapins, herbe dans chaque cellule avec les règles complètes du TD/TP.
* Mesure et tracer du nombre de proies/prédateurs évoluant au cours du temps.
* Comparaison avec les courbes de Lodka-Voltera (attention avec les règles assez basiques du TD, vous n'obtiendrez probablement pas les mêmes oscillations que les courbes théoriques).

#### Autres idées
Tout autre sujet dérivé des thèmes de l'UE en utilisant une sous-partie des TD/TP comme les particules, “jeu de la vie”, évolution de la couleur des insectes ou vendeurs de glaces. Discutez-en avec votre enseignant rapporteur de projet pour voir si le projet est faisable ou non. Voici des exemples, il y en a d'autres plus bas dans la page.
* Mini-jeu type Early Bird : un lanceur envoie un objet physique (particule) sur des cibles.
* Des variantes de pong ou d'Arkanoid : la balle est physique + collisions.
* Amélioration de l'algorithme de sélection pour l'évolution de caractéristiques.
Voir les listes d'autres idées plus loin.


## Les rendus

Les rendus intermédiaires se font dans les colonnes “MiniProjetDepotSemaineN”. Chaque vendredi à la fin du TP vous déposerez votre mini-projet : soit juste le `.cpp` avec des explications en haut en commentaires, soit un `zip` avec le `.cpp`, les images et le readme.md. Nous vous affecterons un rapporteur dont le rôle sera de vous faire un retour sur votre code pendant les séances de TP, et/ou dans la case TOMUSS “MiniProjetCommentairesSemaineN”. Le rendu final se fait dans la colonne de dépôt TOMUSS “MiniProjetDepotFinal”. Voir la date de rendu dans le tableau de planning. Le fichier `ZIP` final doit contenir :
* le répertoire data avec uniquement les images de votre application ;
* le fichier .cpp ;
* au moins une image capturée de votre application (faites F12 avec grapic et l'image de votre application sera sauvée dans grapic/build/windows/grapic.png).
* un fichier README.md (format markdown, voir plus bas) comportant dans cet ordre
  * votre nom/prénom/numéro d'étudiant
  * l'objectif de l'application et comment l'utiliser (touches, but de l'app), etc, 
  * des explications en français/anglais (et non en code) de comment votre code fait les choses (TRÈS IMPORTANT)
  * l'historique des évolutions depuis le début avec au moins une rubrique par semaine
  * des références vers des documents qui vous ont servis (Wikipedia, etc.) (IMPORTANT)

Votre readme.md est au format Markdown (`.md`). Voir des explications ici : https://markdownlivepreview.com/



## Notation
Il y aura des notes intermédiaires chaque semaine basée sur l'avancement par rapport à la semaine précédente.

Pour avoir 12/20, il faut un programme qui tourne avec un affichage graphique, plus loin que les corrigés qui sont en ligne ! Puir sur 8 points, l'étudiant propose des améliorations.

* ~18+ / 20 : une application qui tourne bien, avec des sciences. Par exemple, un mini-jeu avec une simulation physique comportant une/des balle(s), des ressorts, des interactions avec des forces évoluées (gravité universelle, ressort, etc.). Une simulation proies/prédateurs complète avec les courbes et une comparaison avec les courbes théoriques.
* ~16,17 / 20 : une application qui tourne très bien, avec de la science. Typiquement un mini-jeu avec une ou plusieurs balles physiques, avec des interactions complexes avec les briques et d'autres objets (bon rebond, forces un peu évoluées, etc.).
* ~14,15 / 20 : une application qui tourne bien, très peu de science. Par exemple juste une unique balle. Typiquement un mini-jeu avec une seule balle physique et des interactions simple : un casse-briques simple ou un Pong.
* ~10,14 / 20 : la réalisation d'un TP complet type masse-ressort, vendeur de glace ou évolution des insectes.
* 0..9 : programme peu fonctionnel
(Point en moins) -1 à -3 points : code mal écrit, mal indenté, structure trop compliquée, algorithme compliqué, passage de paramètres mal appropriés, etc.


**ATTENTION** : toute triche détectée entrainera une sanction et probablement un passage devant la section disciplinaire. ChatGPT peut vous aider pour quelques lignes mais pas pour copier/coller une grande partie de votre projet. ChatGPT vous proposera du code "pro" utilisant 100% du C++ moderne alors que dans un soucis pédagogique nous ne vous montrons qu'une sous-partie du langage.



## Autres idées de sujet
LIFAMI est une UE d'initiation qui s'inscrit dans le domaine “Computational_science” (la traduction française n'existe pas vraiment), vous pouvez lire la définition sur Wikepdia : https://en.wikipedia.org/wiki/Computational_science#Methods_and_algorithms. Voici des idées en vrac (documentez-vous avec Wikipedia) :

* Simulation d'incendie (variante du jeu de la vie) : http://cormas.cirad.fr/fr/applica/fireautomata.htm
* Méthodes d'interpolation : cosinus, polynomial. Illustré par un éditeur de courbes 2D : spline, bezier, etc.
* Simulation du trafic routier
* Simulation de la propagation d'un virus sur un territoire (toute ressemblance est fortuite). https://fr.wikipedia.org/wiki/Mod%C3%A8les_compartimentaux_en_%C3%A9pid%C3%A9miologie
* Lancer de rayon avec les intersections droite/sphère/plan et prise en compte de la lumière
* Bruit de Perlin
* Méthode d'intégration en simulation (Runge-Kutta, implicite, etc.)
* Méthode de Monte-Carlo appliquée à un problème (calcul de Pi) : https://interstices.info/la-simulation-de-monte-carlo/
* Météo et simulation
* Température, altitude et pression atmosphérique : trouver l'altitude à partir d'un capteur de pression (https://planet-terre.ens-lyon.fr/article/altitude-methode-barometrique.xml)
* Vitesse de la lumière et vitesse du son , propagation du son dans l'air, dans l'eau : http://culturesciencesphysique.ens-lyon.fr/ressource/son-propagation.xml
* Modéliser la propagation d’une épidémie(modèle SIR)|
* Équations de la relativité restreinte : simulation de deux particules allant à des vitesses différentes et emportant une montre



