#include <Grapic.h>
#include <math.h>
#include <iostream>

using namespace std;
using namespace grapic;

const int DIMW = 500;


// **************************************************
// ***** TD Question 1 et 2
// **************************************************

// Entre 8 et 12 � => 1500 mg, soit le point A(12,1500)
// Entre 25 et 30 � => 700 mg, soit le point B(25,700)
// Appelons f la fonction qui prend en parametre la temp�rature et qui renvoie la concentration en fluor.
// Nous avons f(12) = 1500 et f(25)=700. Entre 12 et 25, nous allons faire une interpolation en utilisant la droite AB.
//
// De mani�re g�n�ral, nous cherchons f(x) = la droite AB.
// La pente de la droite AB est p_AB = (700-1500)/(25-12) ~ -61.53   car rappel : pente = (f(b)-f(a)) / (b-a)
//
// Soit le point X(x,f(x)) un point de la droite AB
// La pente de la droite AX doit �tre identique � la pente de AB car X est sur la droite AB
//
// La pente de AX est p_AX = (f(x)-f(12))/(x-12) = p_AB = -61.53
// donc f(x) = p_AB * (x-12) + f(12) = p_AB * x + 1500 - 12*p_AB
// Pour 17�, nous cherchons f(17)
// T=17� => f(17) ~ 1192

float fluor(float T)
{
    if (T<12) return 1500;
    if (T>25) return 700;
    float p_AB = (700.0-1500)/(25-12);
    float b = 1500-12.0*p_AB;
    float fluor = p_AB*T+b;

    // OU
    //float t = (T-12)/(25-12);               // t parametre qui doit �tre � 0 pour 12 et � 1 pour 25
    //float fluor = (1-t)*1500 + t*700;       // quand T=12 on a t=0 et donc fluor = 1500. Quand T=25 on a t=1 et donc fluor=700

    return fluor;
}


// **************************************************
// ***** TD Question 3 � 6
// **************************************************

// 13h30 => � 13h T=19� et � 14h T=16� donc � 13h30 on a T = (19+16)/2
// 10h20 => � 10h T=14� et � 11h T=16�
// 20min correspond � t=20/60, 20 soixante ieme d'heure
// on fait une moyenne pond�r�e entre 14� et 16� avec comme poids (1-t) et t
// donc T = (1-(20/60)) * 14 + (20/60) * 16 = (40/60) * 14 + (20/60) * 16

// Question 5
float Temperature(float T[10], int h, int m)
{
    float poids = m/60.0;
    int indice = h-9;
    float t;
    t = (1-poids) * T[indice] + poids*T[indice+1];
    return t;
}



// **************************************************
// ***** TD Question 7 � 9
// **************************************************

// ***** TD Question 7
//
// La droite passant par A et B a pour �quation param�trique : A + t. AB
//
// La temp�rature en fonction de t est
// T(t) = Ta*(1-t) + Tb*t
// Ceci est une moyenne pond�r�e entre Ta et Tb
//
// Quand t=0, on est sur A et T=Ta
// Quand t=1 on est sur B et T=Tb
// Quand t=0.5 on est au milieu entre A et B et T=0.5*Ta + 0.5*Tb
//


// Point = structure Complex du TD1
struct Point
{
    float x,y;
};

Point make_point(float x, float y)
{
    Point p;
    p.x = x;
    p.y = y;
    return p;
}

Point make_point_exp(float r, float a)
{
    Point p;
    float ar = a*M_PI / 180.f;
    p.x = r*cos( ar );
    p.y = r*sin( ar );
    return p;
}

Point operator+(Point a, Point b)
{
    Point r;
    r.x = a.x + b.x;
    r.y = a.y + b.y;
    return r;
}

Point operator-(Point a, Point b)
{
    Point r;
    r.x = a.x - b.x;
    r.y = a.y - b.y;
    return r;
}

Point operator*(float a, Point b)
{
    Point r;
    r.x = a * b.x;
    r.y = a * b.y;
    return r;
}

Point operator*(Point a, Point b)
{
    Point r;
    r.x = a.x * b.x - a.y * b.y;
    r.y = a.x * b.y + a.y * b.x;
    return r;
}


// ***** TD Question 8
struct Color
{
    int r;		// Des int pour pouvoir depasser 255 lors des additions mais ce n'est pas tr�s joli, ni conventionnelle
    int g;
    int b;
};

Color make_color(unsigned char r, unsigned char g, unsigned char b)
{
    Color c = {r,g,b};
    return c;
}

Color operator+(Color a, Color b)
{
    Color r;
    r.r = a.r + b.r;
    r.g = a.g + b.g;
    r.b = a.b + b.b;
    return r;
}

Color operator*(float a, Color b)
{
    Color r;
    r.r = a * b.r;
    r.g = a * b.g;
    r.b = a * b.b;
    return r;
}

Color colorTemperature(float temperature)
{
    Color froid = make_color(0,0,255);
    Color chaud = make_color(255,0,0);
    const float Tfroid = -30;
    const float Tchaud = 40;
    float t = (temperature-Tfroid)/(Tchaud-Tfroid);     // on se place entre 0 et 1
    Color result = (1-t)*froid + t*chaud;               // on utilise t comme poids de la moyenne pond�r�e (=interpolation)
    return result;
}

void draw_line(Point A, Point B, Color CA, Color CB)
{
    float t;
    Point prec = A, cur;
    Color Ccur;
    int i;
    for(i=0;i<=100;++i)
    {
        t = float(i)/100;
        cur = (1-t)*A + t*B;            // interpolation point
        Ccur = (1-t)*CA + t*CB;         // interpolation couleur
        color( Ccur.r, Ccur.g, Ccur.b);
        line(prec.x,prec.y,cur.x,cur.y);
        prec = cur;
    }
}

void draw_square(Point A, Point C, Color CA, Color CB, Color CC, Color CD)
{
    float tx, ty;
    int i,j;

    for(i=A.x;i<=C.x;++i)
    {
        tx = (float(i)-A.x)/(C.x-A.x);
        Color CE = (1-tx)*CA + tx*CB;
        Color CF = (1-tx)*CD + tx*CC;
        for(j=A.y;j<=C.y;++j)
        {
            ty = (float(j)-A.y)/(C.y-A.y);
            Color CG = (1-ty)*CE + ty*CF;
            put_pixel(i,j, CG.r, CG.g, CG.b);
        }
    }
}

void draw_triangle(Point A, Point B, Point C, float Ta, float Tb, float Tc)
{
    // TODO
}



// **************************************************
// ***** TP
// ***** TP
// ***** TP
// ***** TP
// ***** TP
// **************************************************


// **************************************************
// ***** TP Question 1
// **************************************************

float interpolation( float V1, float V2, float t)
{
    return (1-t)*V1 + t*V2;
}

// ***** TP Question 2 : voir TD plus haut
// ***** TP Question 3 : voir TD plus haut



// **************************************************
// ***** TP Question 4 � 8
// **************************************************
// ================================= Polygon ===================================================
struct Polygon
{
    int n;
    Point p[100];
};


void draw_polygon(Polygon p)            // la question du TD
{
    int i,in;
    for(i=0;i<p.n;++i)
    {
        in = (i+1)%p.n;;
        line( p.p[i].x, p.p[i].y, p.p[in].x, p.p[in].y);
    }
}

void interpo_polygon( Polygon& p, Polygon a, Polygon b, float t)        // la question du TD
{
    int i;
    for(i=0;i<p.n;++i)
    {
        p.p[i] = (1.f-t)*a.p[i] + t*b.p[i];
    }
}
#include "../../data/zzz_lifami_td2/poly1.h"
#include "../../data/zzz_lifami_td2/poly2.h"
#include "../../data/zzz_lifami_td2/poly3.h"
#include "../../data/zzz_lifami_td2/poly4.h"


// ================ Polygon: functions to generate the polygons (CE N EST PAS DANS LE TD/TP,
// ================ ces fonctions ont ete utilisees pour generer les donnees) ==================
void initCircle(Polygon& p, int n)            // Place n points en forme de cercle
{
    p.n = n;
    int i;
    float a;
    for(i=0;i<n;++i)
    {
        a = 2.f * M_PI*float(i)/(n-1);
        p.p[i].x = DIMW/2 + 400*cos(a);
        p.p[i].y = DIMW/2 + 400*sin(a);
    }
}

void match(Polygon& p, const Image& im)     // lasso : rapproche les points vers le centre sauf si il rencontre un bord de l'image
{
    Point G = {DIMW/2, DIMW/2};
    float t;
    int i;
    for(i=0;i<p.n;i++)
    {
        t = 1.f/200;
        Point P = (1-t)*p.p[i] + t*G;
        if (P.x<0) P.x=0;
        if (P.y<0) P.y=0;
        if (P.x>=image_width(im)) P.x=image_width(im)-1;
        if (P.y>=image_height(im)) P.y=image_height(im)-1;

        if (image_get(im,int(P.x), int(P.y), 0) ==255 ) p.p[i] = P;
    }
}

void print_polygon(Polygon p)       // sauve dans un fichier le polygone sous forme de code C++ que les �tudiants porront copier/coller
{
    int i,in;
    FILE* f = fopen("polygon.txt","w");
    fprintf(f,"void init(Polygon& p)\n{\n");
    fprintf(f,"\tp.n=%d;\n",p.n);
    for(i=0;i<p.n;++i)
    {
        fprintf(f,"\tp.p[%d]={%f,%f};\n", i,p.p[i].x,p.p[i].y);
    }
    fprintf(f,"}\n");
    fclose(f);
}




// **************************************************
// ***** TP Question 10 � 14
// ***** Les donn�es de cette partie ne sont pas des donn�es r�elles
// ****** (j'en avais mais elles �taient trop subtiles pour �tre facilement comprises, et trop volumineuses pour un calcul rapide).
// **************************************************
// ================================= Temperature ===================================================
struct WeatherStation
{
    float lon,lat;          // Coord Monde
    int x,y;                // Coord Windows
    float temperature;
};

struct WorldTemperature
{
    int n;
    WeatherStation ws[1000];
    Image world;
    //Image temperature;
};

#include "../../data/zzz_lifami_td2/temperatureInit.h"

float interpolation_temperature(WorldTemperature wt, int x, int y)
{
    int i;
    float sw = 0.f;
    float s = 0.f;
    float w,d;
    for(i=0;i<wt.n;++i)      // Nous allons calculer la moyenne pod�r� des temp�ratures de toutes les stations m�teo. Le poids sera l'inverse de la distance.
    {
        d = ( (wt.ws[i].x-x)*(wt.ws[i].x-x) + (wt.ws[i].y-y)*(wt.ws[i].y-y) );   // distance entre le point(x,y) et la station m�teo
        w = 1.f / (.2f + d);                                                     // poids donn� � cette station : le 0.2+d est pour �viter la division par 0
        sw += w;
        s += w * wt.ws[i].temperature;
    }
    return s/sw;
}

// Fonction qui renvoie la couleur � partir d'une temp�rature
// Vous pouvez utilisez la fonction colorTemperature du TD qui interpole du bleu au rouge.
// Ici cette fonction interpole entre 3 couleurs : du bleu au blanc, puis du blanc au rouge entre m et M
Color colorFromTemperature(float temperature)
{
    Color blue = { 0,0,255};
    Color red = { 255,0,0};
    Color white = { 255,255,0};
    float m=-30.f;
    float M=40.f;
    Color result;
    if (temperature<m) temperature=m;
    if (temperature>M) temperature=M;
    float interpo = (temperature-m)/(M-m);
    if (interpo<0.5f)
    {
        interpo*=2;
        result = (1.f-interpo)*blue + interpo*white;
    }
    else
    {
        interpo = (interpo-0.5f)*2.f;
        result = (1.f-interpo)*white + interpo*red;
    }
    return result;
}

// Proc�dure qui convertit des coordonn�es longitude,latitude en coordonn�e pixel dana la fenetre (donc en 0 et DIMW-1)
void LongLatVersXY(float lon, float lat, int& x, int& y)
{
    x = ((lon + 180.f)/360.f) * (DIMW-1);     // longitude entre -180� et + 180� � ramener entre 0 et DIMW-1
    y = ((lat +  90.f)/180.f) * (DIMW-1);     // latitude entre -90� et + 90� � ramener entre 0 et DIMW-1
}

void init(WorldTemperature& wt)
{
    wt.world = image("data/zzz_lifami_td2/world.png");
    //wt.temperature = image( DIMW, DIMW );

    initFromData(wt);

    int i;
    for(i=0;i<wt.n;++i)
    {
        LongLatVersXY( wt.ws[i].lon, wt.ws[i].lat, wt.ws[i].x, wt.ws[i].y );
    }

    cout<<"Number of sampling temperature: "<<wt.n<<endl;
}


void draw(WorldTemperature wt)
{
    image_draw( wt.world, 0,0, DIMW-1, DIMW-1 );
    //image_draw( wt.temperature, 0, 0, DIMW-1, DIMW-1);

    int i;
    Color col;
    for(i=0;i<wt.n;++i)
    {
        color(255,0,0,255);
        circleFill( wt.ws[i].x, wt.ws[i].y, 4);
    }

    int x,y;
    Color colTemperature;
    Color colWorld;
    float temperature;
    for(x=0;x<DIMW;++x)
        for(y=0;y<DIMW;++y)
        {
            temperature = interpolation_temperature(wt,x,y);
            colTemperature = colorFromTemperature(temperature);
            //image_set( wt.temperature, x, y, col.r, col.g, col.b, 155);     // 155 = opacit� pour laisser apparaitre la carte du monde en dessous
            colWorld.r = image_get(wt.world,x,y,0);
            colWorld.g = image_get(wt.world,x,y,1);
            colWorld.b = image_get(wt.world,x,y,2);
            col = 0.5*colTemperature + 0.5*colWorld;
            color(col.r, col.g, col.b);
            point( x, y );
        }


//    float temperature;
//    if (isMousePressed(SDL_BUTTON_LEFT))
//    {
//        int a,b;
//        mousePos(a, b);
//        temperature = interpolation_temperature(wt,a,b);
//        color(255,0,0,255);
//        print(300, 5, temperature);
//
//        int R,G,B;
//        R = image_get( wt.temperature, a,b,0);
//        print(400, 5, R);
//        G = image_get( wt.temperature, a,b,1);
//        print(430, 5, G);
//        B = image_get( wt.temperature, a,b,2);
//        print(460, 5, B);
//    }
}


float interp(float a, float b, float t)
{
    return (1-t)*a + t*b;
}


void draw_spiral( float cx, float cy, float ra, float aa, float rb, float ab)       // interpolation between radius a (ra), angle (aa) to radius rb, angle rb //10, 0, 200, 360*4)
{
    int i;
    const int N = 1000;
    Point Pold, P, centre;
    centre = make_point(cx, cy);
    float r,a,t;
    r = ra;
    a = aa;
    P = make_point_exp(r,a) + centre;
    Pold = P;
    for(i=0;i<=N;i++)
    {
        t = float(i)/N;
        r = interp(ra, rb, t);
        a = interp(aa, ab, t);
        P = make_point_exp(r,a) + centre;
        line( Pold.x, Pold.y, P.x, P.y);
        Pold = P;
    }
}







int main(int , char** )
{
    bool stop=false;

	winInit("Interpolation !", DIMW, DIMW);
    backgroundColor( 240, 230, 255 );

    Menu menu;
    menu_add( menu, "Color interpolation");
    menu_add( menu, "Match polygon");
    menu_add( menu, "Save polygon");
    menu_add( menu, "Morphing polygon");
    menu_add( menu, "Temperature");
    menu_add( menu, "Spiral");


    // === Polygon
    Polygon p;
    initCircle(p, 90);
    //Image im = image("../../data/zzz_lifami_td2/coquillage2.png");
    //Image im = image("../../data/zzz_lifami_td2/coquillage1.png");
    //Image im = image("../../data/zzz_lifami_td2/poisson.png");
    Image im = image("../../data/zzz_lifami_td2/baleine.png");
    Polygon po[3];
    initPoly1(po[0]);
    initPoly3(po[1]);
    initPoly4(po[2]);
    float t;

    // === Temperature
    WorldTemperature wt;
    init(wt);

	while( !stop )
    {
        winClear();
        switch( menu_select(menu) )
        {
            case 0:
                draw_line( make_point(300,300), make_point(450,450), colorTemperature(-30), colorFromTemperature(40) );
                draw_line( make_point(100,300), make_point(50,450), make_color(0,0,255), make_color(255,0,0) );
                draw_square( make_point(300,100), make_point(450,250), colorTemperature(-30), colorTemperature(0),
                                colorTemperature(40), colorFromTemperature(20) );
                draw_square( make_point(150,170), make_point(250,270), make_color(0,0,0), make_color(255,0,0), make_color(0,255,0), make_color(0,0,255) );
                break;
            case 1:
                image_draw(im, 0,0);
                match(p, im);
                color(255,0,0);
                draw_polygon(p);
                break;
            case 2:
                print_polygon(p);
                break;
            case 3:
                t = (sin( 0.5f*elapsedTime() )+1);
                if (t>1)
                    interpo_polygon( p, po[1], po[2], t-1.f);
                else
                    interpo_polygon( p, po[0], po[1], t);
                draw_polygon(p);
                break;
            case 4:
                draw(wt);
                break;
            case 5:
                draw_spiral( 250, 250, 10, 0, 200, 360*4);
                break;
        }

        menu_draw( menu );
        stop = winDisplay();
            }
    winQuit();
	return 0;
}
