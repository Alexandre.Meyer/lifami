

#include <iostream>
#include <Grapic.h>
#include <cmath>
using namespace grapic;
using namespace std;

const int DIMW = 500;
const float FRICTION = 0.6f;
const float G = 9.81f;


// =========== TD Question 1
// a) 30 km/h = 30*1000/3600 = 8.33 m/s
// En 10s elle aura avanc�e de 83.3 m, elle sera � 1.2833 km
// b) Elle acc�l�re. Apr�s 10s, elle aura fait +10m/s donc elle roulera 18.33 m/s
// A 18.33 m/s, apr�s 10s elle aura parcouru  183.3m
//
//
// =========== TD Question 2
// Ici acc�l�ration = constante que l'on cherche = A
// v(t) = Int�grale de a(t) = A.t + B
// v(0) = 0 = B
// donc v(t) = A.t
//
// x(t) = Int�grale de v(t) = (A/2).t^2 + C
// x(0) = C = 0 donc x(t) = (A/2).t^2
// x = 100 = (A/2).t^2 <=> A = 100*2/t^2 = 100*2/(11.05*11.05)) = 1.637 m.s^-2


// =========== TD Question 3
// Newton F = ma, ici la seule F est mg donc mg = ma <=> g=a(t)=v'(t)
// v(t) = Int�grale de a(t) = g.t + constante
// v(0) = 0 = constante
// donc v(t) = g.t
//
// x(t) = In�grale de v(t) = (g/2).t^2 + constante
// x(0) = constante = 0 donc x(t) = (g/2).t^2
// x = 365 = (g/2).t^2 <=> t = sqrt(365*2/g) = sqrt(365*2/9.81) = 8.62s
//
// vitesse � t=8.62s est v(8.62) = g * 8.62 = 84.62 m/s = 304.6 km/h
// On n�glige les frottement


// =========== TD Question 4
// t=0min, x_0 = 0
// t=1min, v[t-1]=35km/h=9.72m/s   et x_1 = 9.72*60 = 583.2 m
// t=2min, v[t-1]=45km/h=12.5m/s   et x_2 = x_1 + 12.5*60 = 583.2 + 12.5*60 = 1333.2 m
// ...

// =========== TD Question 5
float KmHVersMS(float kh)
{
	return kh*1000/3600;
}

float distance(float vitesse[100], int t) // t=temps est entier, on ne fait pas d'interpolation
{
	float d=0;
	int i;
	for(i=0;i<t;i++)
		d = d + KmHVersMS(vitesse[i])*60;
	return d;
}

// =========== TD Question 6
// F = m.a
// =========== TD Question 7
// C'est comme la pi�ce qui tombe donc voir question 3
// En 2D on a v=(vx,vy) et p=(x,y).
// On fait donc le calcul pour X ET pour Y
//
//
// De mani�re discr�te on a
// a = d�riv�e de la vitesse = (v(t+dt)-v(t))/dt
// et
// v = d�riv�e de la position = (p(t+dt)-p(t))/dt
//
// Donc p(t+dt) = nouvelle position = p(t) + v(t).dt
//
// Et v(t+dt) = nouvelle vitesse = v(t) + a(t).dt
// donc si a(t) = F/m car F=m.a
// On a  v(t) = v(t) + dt.F/m





struct Vec2
{
	float x, y;
};

struct Particle
{
	Vec2 p;         // position(p.x, p.y)
	Vec2 v;         // vitesse en m/s (v.x,v.y)
	Vec2 f;         // force en N (f.x, f.y)
	float m;        // masse en kg
};





// ==== pas demand� en TD/TP mais tr�s important pour le DEBUG
void print(Particle& p)
{
    cout<<" p=("<<p.p.x<<","<<p.p.y<<") v=("<<p.v.x<<","<<p.p.y<<") m="<<p.m<<endl;
}


// ==== les operator sur le Vec2 (idem Complex sauf pour operator* qui n'est pas necessaire l� et devait faire un produit scalaire
Vec2 make_vec2(float x, float y)
{
    Vec2 v;
    v.x = x;
    v.y = y;
    return v;
}

Vec2 operator+(const Vec2& a, const Vec2& b)
{
	Vec2 r;
	r.x = a.x + b.x;
	r.y = a.y + b.y;
	return r;
}

Vec2 operator-(const Vec2& a, const Vec2& b)
{
	Vec2 r;
	r.x = a.x - b.x;
	r.y = a.y - b.y;
	return r;
}

Vec2 operator+=(Vec2& a, const Vec2& b)
{
	a.x += b.x;
	a.y += b.y;
	return a;
}

Vec2 operator*(float a, const Vec2& b)
{
	Vec2 r;
	r.x = a * b.x;
	r.y = a * b.y;
	return r;
}

float norm(const Vec2 v)
{
	return sqrt(v.x*v.x + v.y*v.y);
}



void updateParticle(Particle& part)
{
    const float dt = 0.01;
    if (part.m>0)
    {
        part.v = part.v + (dt/part.m)*part.f;        // mise � jour de la vitesse
        part.p = part.p + dt*part.v;                 // mise � jour de la position (advect)
        part.f.x = 0;
        part.f.y = 0;
    }
}




// ================================  MODE=GRAVITE SUR TERRE, Collision avec le sol et les murs

void collision(Particle& part)
{
	int i;
		if (part.p.x < 0)
		{
			part.p.x = -part.p.x;
			part.v.x = -part.v.x;
			part.v = FRICTION * part.v;
		}

		if (part.p.y < 0)
		{
			part.p.y = -part.p.y;
			part.v.y = -part.v.y;
			part.v = FRICTION * part.v;
		}

		if (part.p.x >= DIMW)
		{
			part.p.x = DIMW-(part.p.x-DIMW);
			part.v.x = -part.v.x;
			part.v = FRICTION * part.v;
		}

		if (part.p.y >= DIMW)
		{
			part.p.y = DIMW - (part.p.y - DIMW);
			part.v.y = -part.v.y;
			part.v = FRICTION * part.v;
		}
}


void computeParticleForceGravityEarth(Particle& part)
{
    part.f = part.f + make_vec2(0, -part.m * G);
}

void initOnEarth(Particle& part)
{
		part.m = 1.0;      // 1kg

		part.p.x = frand(DIMW/2-10, DIMW/2+10 );        // position au hasard dans un rectangle centr� de taille 20
		part.p.y = frand(DIMW/2-10, DIMW/2+10 );

		part.v.x = frand(-80.f, 80.f);                  // vitesse au hasard dans l'intervale de direction (-80..80, -4..80) donc plut�t vers haut
		part.v.y = frand(-4.f, 80.f);
}





// =================================== FIN =============================================


void updateOnearth(Particle& part)
{
    computeParticleForceGravityEarth(part);
    updateParticle(part);
    collision(part);
}




void drawOnEarth(Particle& part)
{
	color( 255, 0, 0 );
	int i=0;
    color(200+i*10, 220, 250 - i*100);
    circleFill( part.p.x, part.p.y, 4);

    if (isKeyPressed('p'))
        print(part);
}


int main(int , char ** )
{
    Particle part;
    bool stop=false;
	winInit("Particles", DIMW, DIMW);
    setKeyRepeatMode(true);
	initOnEarth(part);

    Menu menu;
    menu_add( menu, "Init" );
    menu_add( menu, "Simulation");

    menu_setSelect(menu, 0);
    backgroundColor( 100, 50, 200 );

	while( !stop )
    {
        winClear();
        switch( menu_select(menu))
        {
            case 0:
                initOnEarth(part);      // re-init la particule
                menu_setSelect(menu,1); // bascule en mode simulation (menu 1)
                break;
            case 1:
                drawOnEarth(part);      // dessine
                updateOnearth(part);    // mise � jour de la position/vitesse
                break;

        }

        menu_draw( menu );
        stop = winDisplay();
    }
    winQuit();
	return 0;
}
