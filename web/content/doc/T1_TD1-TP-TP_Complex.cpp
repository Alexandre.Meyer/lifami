#include <Grapic.h>
#include <math.h>
#include <iostream>
using namespace grapic;
using namespace std;

const int DIMW = 800;



// **************************************************
// ***** TD Question 1
// **************************************************

struct Complex
{
    float x,y;
};


// **************************************************
// ***** TD Question 2
// **************************************************

Complex make_complex(float r, float i)
{
    Complex c;
    c.x = r;
    c.y = i;
    return c;
}

Complex make_complex_expo(float r, float theta)
{
    Complex c;
    c.x = r*cos(theta);
    c.y = r*sin(theta);
    return c;
}


// **************************************************
// ***** TD Question 3
// **************************************************

Complex operator+(Complex a, Complex b)
{
    return make_complex( a.x+b.x, a.y+b.y );
//    Complex c = make_complex( a.x+b.x, a.y+b.y );
//    return c;
}

Complex operator-(Complex a, Complex b)
{
    Complex c = make_complex( a.x-b.x, a.y-b.y );
    return c;
}

Complex translate(Complex p, float dx, float dy)
{
    return p + make_complex(dx,dy);
}


// **************************************************
// ***** TD Question 4
// **************************************************
//A( 1,-1) = 1 -1.i  = 1-i
//B( 0, 1) = 0 +1.i  = i
//C(-1,-1) = -1 -1.i = -1-i
//
//lambda=2
//A' = lambda * A = 2*(1-i) = 2-2i   = (2,-2)
//B' = lambda * B = 2*i              = (0, 2)
//C' = lambda * C = 2*(-1-i) = -2-2i = (-2,-2)
// ==> faire le dessin
//
//lambda=0.5
//A' = lambda * A = (0.5, -0.5)
//B' = lambda * B = (0, 0.5)
//C' = lambda * C = (-0.5, -0.5)
// ==> faire le dessin
//
// Multiplication d'un complexe par un r�el = une homoth�tie de centre O = approche (lambda<1) ou �loigne (lambda>1) le point



// **************************************************
// ***** TD Question 5
// **************************************************

Complex operator*(float a, Complex b)
{
    Complex c = make_complex( a*b.x, a*b.y );
    return c;
}
Complex operator*(Complex b, float a)
{
    Complex c = make_complex( a*b.x, a*b.y );
    return c;
}


Complex operator/(Complex b, float d)
{
    Complex c = make_complex( b.x/d, b.y/d );
    return c;
}

Complex scale(Complex p, float cx, float cy, float sc)
{
    Complex tr = make_complex( cx, cy);
    return (sc*(p-tr))+tr;
}



// **************************************************
// ***** TD Question 6 et 7
// **************************************************

// A( 1,-1) = 1 -1.i  = 1-i
// B( 0, 1) = 0 +1.i  = i
// C(-1,-1) = -1 -1.i = -1-i
//
// r = e^(i.theta) avec theta = PI/2 donc une rotation de 90�
// r = cos(PI/2) + i.sin(PI/2) = 0+i = i
//A' = r * A = i*(1-i) = 1+i    = (1,1)
//B' = r * B = i*i = -1         = (-1, 0)
//C' = r * C = i*(-1-i) = 1-i   = (1,-1)
// ==> faire le dessin
// ==> rotation de 90�
//
// ==> ca marche avec n'importe quel theta
// par exemple avec theta=0  r= cos(0)+i.sin(0) = 1 => identite
// par exemple avec theta=PI r= cos(PI)+i.sin(PI) = -1
//
// Multipli� un complexe par un complexe imaginaire pure (r=e^(i.theta) ==> rotation de theta�



// **************************************************
// ***** TD Question 8
// **************************************************

float to_degree(float rad)
{
    return 180.f * rad/M_PI;
}

float to_rad(float deg)
{
    return M_PI*deg/180.f;
}

Complex operator*(Complex a, Complex b)
{
    Complex c = make_complex( a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x );
    return c;
}

Complex rotate(Complex p, float cx, float cy, float theta_deg)
{
    Complex rot = make_complex_expo( 1, to_rad(theta_deg));
    Complex tr = make_complex( cx, cy);
    return ((p-tr)*rot)+tr;
}


float norm(Complex c)
{
    return sqrt( c.x*c.x + c.y*c.y);
}





// **************************************************
// ***** TD Question 9
// **************************************************
// ************************** SOLAR SYSTEM ***************************************************
struct SolarSystem
{
    Complex sun;
    Complex mars;
    Complex earth;
    Complex moon;

};

void init(SolarSystem& ss)
{
    ss.sun = make_complex(DIMW/2, DIMW/2);
    ss.mars = ss.sun + make_complex(30, 0);
    ss.earth = ss.sun + make_complex(80, 0);
    ss.moon = ss.earth + make_complex(15, 0);
}

void draw(SolarSystem ss)
{
    color(255,255,0);
    circleFill( ss.sun.x, ss.sun.y, 10);

    color(255,25,0);
    circleFill( ss.mars.x, ss.mars.y, 4);

    color(0,25,255);
    circleFill( ss.earth.x, ss.earth.y, 5);

    color(125,25,155);
    circleFill( ss.moon.x, ss.moon.y, 2);
}

void update(SolarSystem& ss)
{
    ss.mars = rotate( ss.mars, ss.sun.x, ss.sun.y, .1f);        // Mars tourne autour du soleil d'un angle 0.1 degr� � chaque frame

    Complex moon_local = ss.moon - ss.earth;                   // Position de la lune par rapport � la terre
    ss.earth = rotate( ss.earth, ss.sun.x, ss.sun.y, .03f);     // Terre tourne autour du soleil d'un angle 0.03 degr� � chaque frame
    ss.moon = ss.earth + moon_local;                            // On remet la lune au m�me endroit par rapport � la nouvelle position de la terre

    ss.moon = rotate( ss.moon, ss.earth.x, ss.earth.y, .03f);   // Lune tourne autour de la terre d'un angle 0.03 degr� � chaque frame
}


// **************************************************
// ***** TD Question 10 � 17
// **************************************************
// =============================  POLYGON ============================================
const int POLY_MAX_POINT = 100;
struct Polygon
{
    Complex p[POLY_MAX_POINT];
    int n;
};

Complex CenterOfGravity(Polygon& p)
{
    int i;
    Complex cog = make_complex(0,0);
    for(i=0;i<p.n;++i)
    {
        cog = cog + p.p[i];
    }
    return cog/p.n;
}

void scale(Polygon& p, float sc)
{
    Complex cog = CenterOfGravity(p);
    int i;
    for(i=0;i<p.n;++i)
    {
        p.p[i] = scale( p.p[i], cog.x, cog.y, sc);
    }
}

void rotate(Polygon& p, float angle)
{
    Complex cog = CenterOfGravity(p);
    int i;
    for(i=0;i<p.n;++i)
    {
        p.p[i] = rotate( p.p[i], cog.x, cog.y, angle);
    }
}

void init(Polygon& po)
{
    po.n = 0;
}

void addPoint(Polygon& po, float x, float y)
{
    if (po.n>=POLY_MAX_POINT) return;
    po.p[ po.n ] = make_complex( x, y);
    ++po.n;
}

void print_poly(const Polygon& p)
{
    cout<<"n="<<p.n<<endl;
    for(int i=0;i<p.n;++i)
        cout<<p.p[i].x<<", "<<p.p[i].y<<endl;
}

void draw_polygon(Polygon& po)
{
    int i,in;
    color(255,0,0);
    for(i=0;i<po.n;++i)
    {
        in = (i+1)%po.n;
        line( po.p[i].x, po.p[i].y, po.p[in].x, po.p[in].y );
    }

    if (isMousePressed(SDL_BUTTON_RIGHT))
    {
        int x,y;
        mousePos(x, y);

        if ((po.n==0) || (norm(po.p[po.n-1]-make_complex(x,y))>5.f))
            addPoint(po, x,y);
    }
    if (isKeyPressed(SDLK_UP))
        scale(po, 1.2);
    if (isKeyPressed(SDLK_DOWN))
        scale(po, 0.8);

    if (isKeyPressed(SDLK_LEFT))
        scale(po, -1.f);

    if (isKeyPressed(SDLK_RIGHT))
        rotate(po, 5);         // degre
}




// **************** TP ***********************************************************************************
// **************** TP ***********************************************************************************
// **************** TP ***********************************************************************************

// **************************************************
// ***** TP Question 1 : voir question du TD
// **************************************************


// **************************************************
// ***** TP Question 2 et 3
// **************************************************
// ************************** Sampling ***************************************************
void draw_sampling()
{
    int i,j;
    float a, b;
    Complex c;
    Complex center = make_complex( DIMW/2, DIMW/2);
    color(255,0,0);
    const int MAX = 10;
    for(i=0;i<MAX;++i)
    {
        a = float(DIMW) * i/MAX;
        for(j=0;j<MAX;++j)
        {
            b = float(DIMW) * j/MAX;
            c = make_complex(a,b);
            circleFill( c.x, c.y, 2);
        }
    }

}

// ************************** Sampling EXPO ***************************************************
void draw_sampling_expo()
{
    int i,j;
    float r, theta;
    Complex c;
    Complex center = make_complex( DIMW/2, DIMW/2);
    color(255,0,0);
    const int MAX = 20;
    for(i=0;i<MAX;++i)
    {
        r = 0.5f*DIMW * i/MAX;
        for(j=0;j<MAX;++j)
        {
            theta = 2.f*M_PI*j/MAX;
            c = center + make_complex_expo(r,theta);
            circleFill( c.x, c.y, 1);
        }
    }

}


// **************************************************
// ***** TP Question 8 � 10 : voir question du TD
// **************************************************




// **************************************************
// ***** TP Question 4 � 7
// **************************************************
// ************************** BIRD ***************************************************
struct Bird
{
    Complex c;      // centre de l'oiseau
    float angle;    // angle des ailes en degr�
};

void init(Bird& b)
{
    b.c = make_complex(DIMW/2, DIMW/2);
    b.angle = 15;           // angle au repos en degr�
}

void draw(Bird& b)
{
    color(255,255,0);
    circleFill( b.c.x, b.c.y, 2);

    Complex right = b.c + make_complex(20,0);       // extremite de l'aile droite
    Complex left = b.c + make_complex(-20,0);       // extremite de l'aile gauche
    right = rotate( right, b.c.x, b.c.y, b.angle);  // tourner l'extremit� de l'aile d'un angle b.angle
    left = rotate( left, b.c.x, b.c.y, -b.angle);   // tourner l'extremit� de l'aile d'un angle -b.angle

    // dessine les deux ailes
    line(b.c.x, b.c.y, left.x, left.y);
    line(b.c.x, b.c.y, right.x, right.y);
}

void update(Bird& b)
{
    const float d = 0.1f;
    if (b.c.y > 3) b.c.y-=d;            // fait tomber l'oiseau � chaque fois

    if (isKeyPressed(SDLK_LEFT))   if (b.c.x > 0) b.c.x-=d;
    if (isKeyPressed(SDLK_RIGHT))  if (b.c.x < DIMW) b.c.x+=d;
    if (isKeyPressed(SDLK_UP))
    {
        b.c.y += 2.f*d;                 // fait remonter l'oiseau si KEY_UP
        float t = elapsedTime();
        b.angle = 20.f*cos(50.f*t);     // modifie l'angle des ailes, oscille entre -20 et +20 � (le 50*t sert � acc�l�rer le mouvement)
    }
    else b.angle = 15.f;
}



// **************************************************
// ***** TP Question 11 � 14
// **************************************************
// =============================  JULIA ============================================
const int MAXITE = 200;
int suite_julia(float borne, int maxIte, Complex Z0, Complex C)
{
    Complex Zn=Z0;
    int i;
    i=0;
    do
    {
        Zn = Zn*Zn+C;
        i++;
    }while ( (norm(Zn)<borne) && (i<maxIte) );
    return i;
}

void couleur_julia(int n, unsigned char& r, unsigned char& g, unsigned char& b)
{
    float c = (float(n))/MAXITE;
    r = c*255;
    g = 128 + c*128;
    b = 64  + c*(127+64);
}

void draw_julia()
{
    int i,j,n;
    unsigned char r,g,b;
    Complex C = make_complex(0.32, 0.043);
    float x,y;
    for(i=0;i<DIMW;i+=1)
    {
        for(j=0;j<DIMW;j+=1)
        {
            x = ((float(i))/DIMW)*3 - 1.5;
            y = ((float(j))/DIMW)*3 - 1.5;
            n = suite_julia( 2, MAXITE, make_complex(x,y), C );
            couleur_julia( n, r,g,b);
            put_pixel(i,j,r,g,b);
        }
    }
}




int main(int , char** )
{
    bool stop=false;
	winInit("Complex numbers are cool!!!!", DIMW, DIMW);
    backgroundColor( 10, 20, 120 );

    Menu menu;
    menu_add( menu, "Sampling reg");
    menu_add( menu, "Sampling expo");
    menu_add( menu, "SolarSystem");
    menu_add( menu, "Bird");
    menu_add( menu, "Polygon");
    menu_add( menu, "Julia");

    SolarSystem ss;
    init(ss);

    Bird b;
    init(b);

    Polygon po;
    init(po);

	while( !stop )
    {
        setKeyRepeatMode(true);
        winClear();
        //cout<<menu_select(menu)<<endl;
        switch( menu_select(menu) )
        {
            case 0:
                draw_sampling();
                break;
            case 1:
                draw_sampling_expo();
                break;
            case 2 :
                draw(ss);
                update(ss);
                break;
            case 3 :
                setKeyRepeatMode(true);
                draw(b);
                update(b);
                break;
            case 4:
                setKeyRepeatMode(false);
                draw_polygon(po);
                break;
            case 5:
                draw_julia();
                break;
        }
        menu_draw( menu );
        stop = winDisplay();
    }
    winQuit();
	return 0;
}
