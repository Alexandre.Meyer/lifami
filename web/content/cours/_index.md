---
title: "Cours,TD,TP"
description: "Support de cours,TD,TP"
---


## Support de TD/TP
* [Le PDF avec tous les TD et TP est ici.](../doc/2024_2025_lifami_all.pdf)


## Thème 1 : Nombres complexes 
* Translation, changement d'échelle, rotation
  * Animation d'un système solaire
  * Animation d'un oiseau battant des ailes
  * Fractale (Julia)
* [Les slides des parties nombres complexes et interpolation.](../doc/LIFAMI_CM1_ComplexeInterpolation.pdf)
* [Le corrigé des TD/TP](../doc/T1_TD1-TP-TP_Complex.cpp)
  <img src="../images/complex.gif" width="300" class="center">





## Thème 2 : Interpolation
* Interpolation linéaire et bilinéaire
  * Interpolation de couleur le long d'une droite, remplissage d'un rectangle;
  * Interpolation des formes décrites par des polygones 2D;
  * Interpellation de déplacement le long d'un polygone.
* Pour le CM, voir le PDF du thème 1 plus haut.
* Les fichiers `poly1.h`, etc. sont [ici](https://github.com/ucacaxm/grapic/tree/master/data/lifami_polygon) et aussi dans le dossier `data/lifami_polygon` de Grapic.

* [Le corrigé des TD/TP](../doc/T2_TD2-TP-TP_Interpolation.cpp)
  <img src="../images/interpolation.gif" width="300" class="center">




## Thème 3 : Des applications liées à la physique
* Système de particules, gravité;
* Système masses-ressorts
* [Les slides sont ici](../doc/LIFAMI_CM2a_CultureDeProg.pdf) et [ici](../doc/LIFAMI_CM2b_DeriveeIntegrale_MecaDuPoint.pdf)
* [Un corrigé d'une unique particule](../doc/T3_TD3-TP-TP_ParticlesBasic.cpp)
  <div class="image-container">
    <figure style="display: inline-block; margin-right: 10px;">
      <img src="../images/particle1.gif" alt="particle1" width="300" class="center" />
      <figcaption>Une unique particule</figcaption>
    </figure>

    <figure style="display: inline-block; margin-right: 10px;">
      <img src="../images/particle2.gif" alt="particle1" width="300" class="center" />
      <figcaption>Nuage de particule et des planètes simulées</figcaption>
    </figure>

    <figure style="display: inline-block; margin-right: 10px;">
      <img src="../images/mass-spring.gif" alt="particle1" width="300" class="center" />
      <figcaption>Système masse-ressort</figcaption>
    </figure>
  </div>

## Thème 4 : Des applications liées à la biologie
* Proies/prédateurs simulés;
* Équations de prédation de Lotka-Voltera;
* Algorithmes évolutionnistes simples.
* [Les slides sont ici](../doc/LIFAMI_CM3_Bio.pdf)
  <img src="../images/colroinsects.png" width="300" class="center">


## Thème 5 : Des applications liées à l'économie
* Simulation en économie : Loi de Hotelling, simulation des vendeurs de glace
* Le CM porte sur des révisions
  <img src="../images/icescream.gif" width="300" class="center">






## Des anciens sujets pour réviser
[Des sujets passés en vrac.](http://liris.cnrs.fr/alexandre.meyer/teaching/LIFAMI/examens)


* Simulation de la circulation de voiture avec embouteillage en accordéon apparaissant automatiquement
  * <img src="../images/trafic.gif" width="300" class="center">

* Bruit de Perlin pour générer la silhouette de terrain formant un paysage
  * [La vidéo de réalisation de ce sujet](https://www.youtube.com/watch?v=BIXY3H4J19I)
  <img src="../images/perlin.png" width="300" class="center">


* Simulation de la surface 1.5D de l'eau en mouvement
  * <img src="../images/fluid1.gif" width="300" class="center">


* Simulation de l'équation de la chaleur sur une grille régulière 2D
  * Équation de la chaleur : une grille avec source de chaleur, mur, air ⇒ évolution de la température
  * Notion intuitive du Laplacien : une case veut être de la même température que la moyenne de ses voisines
  <img src="../images/heat_simu.gif" width="300" class="center">

* Réaction, diffusion en biologie avec application aux générations de motif d'animaux 
  * Voir également http://www.cc.gatech.edu/~turk/my_papers/reaction_diffusion.pdf
  <img src="../images/react_diff.gif" width="300" class="center">

* Apprentissage statistique simple. L'objectif est de régler par apprentissage, la direction et la force de tir d'un boulet vers sa cible. Un nuage de boulet est tiré. L'algorithme ne garde que ceux qui ont touché leur cible et calcule la moyenne et l'écart type des boulets gagnants.
  * <img src="../images/train.gif" width="300" class="center">

* Un mini-jeu où l'utilisateur doit dessiner des obstacles à une balle pour qu'elle rebondisse vers une cible.
  <img src="../images/toboggan.gif" width="300" class="center">

* Échantillonnage de Poisson ( [Poisson sampling](https://en.wikipedia.org/wiki/Poisson_sampling))
  * Les cercles se repoussent les uns les autres autres pour échantillonner l'espace avec une distribution au sens de Poisson.
  <img src="../images/sampling_fish.gif" width="300" class="center">


* Il y a de nombreuses idées à trouver ici
  * [NetLogo Material Sim Grain Growth model. ](http://ccl.northwestern.edu/netlogo/models/MaterialSimGrainGrowth.). Blikstein, P. and Wilensky, U. (2005). Center for Connected Learning and Computer-Based Modeling, Northwestern University




## Les vidéos des 3 CM (2020)
[Les vidéos](video)
