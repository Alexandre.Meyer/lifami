
---
title: "Grapic"
description: "Grapic"
---




Grapic (Graphics In C/C++) est utilisé pour les TP afin de faciliter l'affichage 2D. Grapic permet facilement d'ouvrir une fenêtre et d'afficher des lignes, des rectangles, des cercles et des images. Pour faire des simulations ou des animations, la boucle principale efface l'écran, bouge les objets, les affiche et recommance. Grapic a été inspiré par le langage [Processing](https://processing.org/).

* [Grapic](https://perso.liris.cnrs.fr/alexandre.meyer/grapic/html/)
* [Le github de Grapic](https://github.com/ucacaxm/grapic)

Grapic fonctionne sous Windows avec Codeblocks, Visual Studio, VSCode et en ligne de commande; sous Linux en ligne de commande, avec VSCode et Codeblocks; sous MacOS avec XCode et en ligne de commande. Si vous avez des difficultés pour le faire tourner demandez à un enseignant et/ou  utiliser [C5](c5.univ-lyon1.fr), mais ne perdez pas de temps..


## Sur votre ordinateur

* Suivez les consignes décrites ici : [Guide pour installer Codeblocks et commencer les TP de LIFAMI avec Grapic](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/LIFAMI/grapic_installation.pdf)
* [La prise en main de Grapic avec la création d'un nouveau projet est illustré dans cette vidéo](http://perso.univ-lyon1.fr/elodie.desseree/LIFAP1/TP/SUJETS/Creer_projet_grapic.mp4)
    <iframe width="400" height="225" src="//perso.univ-lyon1.fr/elodie.desseree/LIFAPI/TP/SUJETS/Creer_projet_grapic.mp4" frameborder="0" allowfullscreen autoplay="false"></iframe>



## Avec C5

Grapic fonctionne avec [C5](c5.univ-lyon1.fr). Il y a un cas particulier avec les images, vous ne pouvez utiliser que les images pré-chargées dans C5. Vous ne pouvez pas utiliser vos propres images.


## La prise en main de Grapic sous MacOS

<iframe width="400" height="225" src="//www.youtube.com/embed/yOMCJMSvHr0" frameborder="0" allowfullscreen autoplay="false"></iframe>

