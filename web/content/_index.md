# LIFAMI Applications en Math/Info

Responsables de l'enseignement : [Alexandre Meyer](https://perso.liris.cnrs.fr/alexandre.meyer) et [Elodie Desseree](https://perso.univ-lyon1.fr/elodie.desseree/LIFAPI)

* Cours dispensé en 1ère année du portail Mathématiques et Informatique au printemps
* Volume horaire : 6h CM, 18h de TD et 36h de TP


<img src="images/lifami_logo.png" width="400" class="center">


## Objectif de l'UE
<p style="text-align:justify;"> L’objectif de ce cours est de montrer des notions élémentaire de mathématique et d'algorithmique/programmation dans un cadre applicatif concret et pratique à l'interface de plusieurs disciplines, comme la mécanique, la biologie, la physique, l'économie, etc. Le cours propose des applications motivantes, toujours codées et illustrées par une visualisation 2D interactive, afin de renforcer les notions de bases de programmation, tout en trouvant un intérêt à assimiler les notions mathématiques dans un contexte applicatif. Ce cours peut-être vu comme un premier cours d'introduction à la science du calcul : <a href="https://en.wikipedia.org/wiki/Computational_science">Computational Science</a>.

Les notions informatique nécessaire au début de l'enseignement sont les bases de l'algorithmique et la programmation : test, boucles, sous-programme, etc. Les notions de mathématiques utilisées et ré-expliquées sont le calcul vectoriel; les notions de dérivée, intégrale, gradient, laplacien; les nombres complexes; interpolation linéaire; différence entre discret et continu; dénombrement; notions de géométrie 2D; etc. L'objectif à la fin de ce cours est que l'étudiant ait acquis une capacité à écrire des programmes plus conséquent, tout en faisant un lien entre les mathématiques, l'informatique et leur utilisation dans des applications scientifiques.</p>


